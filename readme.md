
# CRI Code Club website

This is the repository for the **CRI Code Club website**.   

More information on the website itself can be found on the project's [wiki](https://framagit.org/cricodeclub/website/wikis/home).

>[The CRI](http://cri-paris.org/) (Center for Research and Interdisciplinarity) gives the opportunity to their students to create clubs. A club is similar to a student association, powered by the CRI (fundings, place, etc).
>
>So, we created a club around programming culture in order to open programming for everyone and promote a technical ecosystem in the CRI.

We wanted **to open the creation process of the CRI Code Club website to everyone**, as part of our class *citizen science*.

This project uses [jekyll](http://jekyllrb.com/) as a basis.

## Getting started

#### [Install ruby](https://www.ruby-lang.org/en/documentation/installation/) on your computer / VM

**On Windows**

- With a Ruby Version Manager ([PIK](https://github.com/vertiginous/pik))
- With an installer [Ruby installer for windows](http://rubyinstaller.org/)

**On OSX**
*Mac OSX is provided with ruby by default. It is not the latest stable version of ruby wich is installed. So, if you need to update it, you can follow these instructions, otherwise, you can skip them.*

- With Ruby Version Manager ([RVM](https://rvm.io/rvm/install))
- With [Homebrew](http://brew.sh/) run `brew install ruby`
	

**On Debian / Ubuntu**

- With Ruby Version Manager ([RVM](https://rvm.io/rvm/install))
- With the package manager apt run `sudo apt-get install ruby-full`

#### Install [jekyll](http://jekyllrb.com/)

Once ruby installed on your computer, use [gem](https://rubygems.org/) (the ruby package manager, provided with ruby by default) to install the ruby package called jekyll.

Run `gem install jekyll`

**Mac OSX - Potential issue**
If you run into a permission error, run the command `sudo gem install jekyll` to overrun it.
*Always be careful when you run a command with sudo*

#### Clone the repository

*I assume that you already have [git](https://git-scm.com/) installed on your computer*

run `git clone https://framagit.org/cricodeclub/website.git`

## Contribute to the project

All contributions are welcome! 

- Take a look at the issues section, solve one or create one. 
- Send us a [pull request](https://help.github.com/articles/fork-a-repo/).
- Send us a feedback/support email cricodeclub[at]gmail.com.

**Please keep in mind**

- the issues section is here for issues, not for support stuff. 
- A bug-report should be in the issues section. 
- A pull request should respect the code style and all rules that are applied along the project.

